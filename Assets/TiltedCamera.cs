using System.Collections;
using System.Collections.Generic;
using Gameplay;
using Pathfinding;
using UnityEngine;

public class TiltedCamera : MonoBehaviour
{
    public float StartZoom;
    public PathNodeGrid PathNodeGrid;
    private Transform m_CameraTransform;

    private IEnumerator Start()
    {
        while (PathNodeGrid != null && PathNodeGrid.Nodes.Count <= 0)
        {
            yield return null;
        }

        //Get the camera's transform
        m_CameraTransform = Camera.main.transform;

        //Move the camera to the start position
        var v = (PathNodeGrid.Nodes.Count / 2f) + (PathNodeGrid.Dimension.x / 2f);
        var offset = new Vector3(PathNodeGrid.OffsetSize.x / 2, 0f, PathNodeGrid.OffsetSize.y / 2);
        m_CameraTransform.position = PathNodeGrid.Nodes[Mathf.CeilToInt(v)].transform.position - offset;

        //Rotate the camera to the correct rotation
        m_CameraTransform.eulerAngles = new Vector3(45f, 0f, 0f);

        //Zoom the camera to the initial zoom
        m_CameraTransform.Translate(-Vector3.forward * StartZoom);

        Camera.main.orthographicSize = StartZoom;
    }
}
