﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace StateMachine
{
    /// <summary>
    /// Enum StackBehaviour
    /// </summary>
    [System.Serializable] 
    public enum StackBehaviour
    {
        /// <summary>
        /// The pop
        /// </summary>
        Pop,
        /// <summary>
        /// The push
        /// </summary>
        Push,
        /// <summary>
        /// The pop then push
        /// </summary>
        PopThenPush,
    }

    public abstract class StackStateMachine : MonoBehaviour
    {
        /// <summary>
        /// The states
        /// </summary>
        protected Dictionary<uint, State> m_States;
        /// <summary>
        /// The state stack
        /// </summary>
        protected Stack<State> m_StateStack;
        /// <summary>
        /// The stack behaviour
        /// </summary>
        public StackBehaviour Behaviour { protected get; set; }
        /// <summary>
        /// The previous state
        /// </summary>
        public State PreviousState { get; protected set; }
        /// <summary>
        /// The current state
        /// </summary>
        public State CurrentState { get; protected set; }

        public delegate void StateUpdate(int previous, int current);
        public event StateUpdate OnStateUpdate;

        public virtual void Awake()
        {
            PreviousState = null;
            m_StateStack = new Stack<State>();
            m_States = new Dictionary<uint, State>();
            Behaviour = StackBehaviour.PopThenPush;
        }

        public bool IsOnTop(uint state) => CurrentState && CurrentState.Get() == state;
        public bool WasOnTop(uint state) => PreviousState && PreviousState.Get() == state;
        
        public State SetState(uint state) => m_States[state];
        public void AddOnTop(uint state) => TransitionTo(SetState(state));

        protected void TransitionTo(State state)
        {
            if (state == null || m_StateStack == null) return;

            if (Behaviour != StackBehaviour.Push && m_StateStack.Count > 0)
            {
                PreviousState = CurrentState;
                m_StateStack.Pop();
            }

            if (Behaviour == StackBehaviour.Pop) return;
            
            m_StateStack.Push(state);
            CurrentState = m_StateStack.Peek();
            CurrentState.Fsm = this;

            if (!WasOnTop(state.Get()))
            {
                OnStateUpdate?.Invoke(PreviousState ? (int)PreviousState.Get() : -1, (int)CurrentState.Get());
            }
        }

        public void Run() => TransitionTo(m_StateStack?.Peek()?.Do());
    }

    public abstract class StackStateMachine<S, E> : StackStateMachine
        where S : State 
        where E : Enum
    {
        [SerializeField] protected List<S> m_Container;
        [field: SerializeField] public E DefaultState { protected get; set; }

        public override void Awake()
        {
            base.Awake();

            foreach (var s in m_Container)
            {
                var state = ScriptableObject.Instantiate(s) as State;
                m_States.Add(state.Get(), state);
            }
        }
    }
}