﻿using UnityEngine;

namespace StateMachine
{
    public abstract class State : ScriptableObject
    {
        public StackStateMachine Fsm { protected get; set; }

        public abstract uint Get();
        public abstract State Do();

        public virtual void Enter() {}
        public virtual void Exit() {}
    }

    public abstract class State<FSM> : State where FSM : StackStateMachine
    {
        public new FSM Fsm { protected get => (FSM)base.Fsm; set => base.Fsm = value; }
    }
}