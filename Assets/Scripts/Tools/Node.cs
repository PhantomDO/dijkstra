﻿using System.Collections.Generic;
using UnityEngine;

namespace Tools
{
    public class Node : MonoBehaviour
    {
        [field: SerializeField] public List<Node> Neighbors { get; private set; }
        public int GridIndex { get; set; }
        protected virtual void Awake()
        {
            Neighbors = new List<Node>();
        }
    }
}