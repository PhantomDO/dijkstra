﻿namespace Tools
{
    [System.Serializable]
    public class Link
    {
        public Node A, B;
        
        public bool Contains(Node a, Node b)
        {
            return (a == A && b == B) || (b == A && a == B);
        }
    }
}