﻿using System.Collections.Generic;
using UnityEngine;

namespace Tools
{
    [System.Serializable]
    public abstract class Path
    {
        [field: SerializeField] public Color Color { get; private set; }
        [field: SerializeField] public List<Node> Nodes { get; private set; }

        protected Path()
        {
            Nodes = new List<Node>();
        }

        protected Path(Node[] nodes)
        {
            Nodes = new List<Node>();
            if (nodes != null)
            {
                Nodes.AddRange(nodes);
            }
        }
        
        public Node Start => Nodes[0];
        public Node End => Nodes[Nodes.Count - 1];
        
        public void SetColor(Gradient gradient, bool finished = false)
        {
            int i = 0;
            for (int n = 0; n < Nodes.Count - 1; n++)
            {
                var node = Nodes[n];
                var neighbor = Nodes[n + 1];
                if (node.Neighbors.Contains(neighbor) && neighbor.Neighbors.Contains(node))
                {
                    i++;
                }
            }

            if (i == Nodes.Count - 1)
            {
                Color = finished ? Color.clear : gradient.Evaluate(1f);
                Debug.Log($"Settings path color to : {Color}");
            }
            else
            {
                Debug.LogError($"The path is not long enough..");
            }
        }
    }
}