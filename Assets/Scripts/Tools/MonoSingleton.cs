﻿using System;
using UnityEngine;

namespace Tools
{
    public class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
    {
        protected MonoSingleton() { }

        public static T Instance { get; protected set; }

        protected virtual void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
                throw new SystemException("An instance of this singleton already exist");
            }
            else
            {
                Instance = (T) this;
            }
        }
    }
}