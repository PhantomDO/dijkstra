﻿using System.Collections.Generic;
using Tools;
using UnityEngine;

namespace Tools
{
    public abstract class NodeGrid<N, L> : MonoBehaviour
        where N : Node
        where L : Link
    {
        [SerializeField] protected List<N> m_Nodes;
        public List<N> Nodes => m_Nodes;

        [SerializeField] protected LinkList<L> m_Links;
        public LinkList<L> Links => m_Links;

        protected Transform m_NodesContainer, m_LinksContainer;

        public virtual void Awake()
        {
            m_NodesContainer = new GameObject("Nodes").transform;
            m_NodesContainer.transform.parent = transform;
            m_LinksContainer = new GameObject("Links").transform;
            m_LinksContainer.transform.parent = transform;
        }
        
        public abstract void Initialize();
        protected abstract void CreateLink(int nodeIndex, int neighborIndex);
        protected abstract void DestroyLink(L link);

        /// <summary>
        /// Add a link between 2 nodes
        /// </summary>
        /// <param name="nodeIndex">the first node</param>
        /// <param name="neighborIndex">the second node</param>
        public void AddLink(int nodeIndex, int neighborIndex)
        {
            if (nodeIndex != neighborIndex)
            {
                var node = m_Nodes[nodeIndex];
                var neighbor = m_Nodes[neighborIndex];

                if (node != null && neighbor != null && 
                    !node.Neighbors.Contains(neighbor) && !neighbor.Neighbors.Contains(node))
                {
                    node.Neighbors.Add(neighbor);
                    neighbor.Neighbors.Add(node);
                    
                    bool alreadyExist = false;
                    foreach (var link in m_Links)
                    {
                        if (link.Contains(node, neighbor))
                        {
                            alreadyExist = true;
                            break;
                        }
                    }

                    if (!alreadyExist)
                    {
                        CreateLink(nodeIndex, neighborIndex);
                    }
                }
            }
        }

        public void RemoveLink(int nodeIndex, int neighborIndex)
        {
            if (nodeIndex != neighborIndex && nodeIndex >= 0 && neighborIndex >= 0)
            {
                var node = m_Nodes[nodeIndex];
                var neighbor = m_Nodes[neighborIndex];

                if (node != null && neighbor != null &&
                    node.Neighbors.Contains(neighbor) && neighbor.Neighbors.Contains(node))
                {
                    node.Neighbors.Remove(neighbor);
                    neighbor.Neighbors.Remove(node);
                    
                    L pathLinkToRemove = null;
                    foreach (var link in m_Links)
                    {
                        if (link.Contains(node, neighbor))
                        {
                            pathLinkToRemove = link;
                            break;
                        }
                    }

                    if (pathLinkToRemove != null)
                    {
                        DestroyLink(pathLinkToRemove);
                        m_Links.Remove(pathLinkToRemove);
                    }
                }
            }
        }
        
        public void RemoveNode(int nodeIndex)
        {
            if (nodeIndex >= 0 && m_Nodes[nodeIndex] != null && m_Nodes[nodeIndex].Neighbors.Count > 0)
            {
                for (int i = 0; i < m_Nodes[nodeIndex].Neighbors.Count; i++)
                {
                    RemoveLink(nodeIndex, i);
                }

                Destroy(m_Nodes[nodeIndex].gameObject);
            }
        }
        
        public void DestroyGrid()
        {
            if (m_Nodes.Count > 0)
            {
                foreach (var node in m_Nodes)
                {
                    if (node)
                    {
                        Destroy(node.gameObject);
                    }
                }
            }
        }

        public int GetIndexAtPosition(Vector3 position)
        {
            int n = -1;
            float distance = Mathf.Infinity;

            for (int i = 0; i < Nodes.Count; i++)
            {
                var node = Nodes[i];
                    
                float d = Vector3.Distance(position, node.transform.position);
                if (d < distance)
                {
                    distance = d;
                    n = i;
                }
            }

            return n;
        }

    }
}