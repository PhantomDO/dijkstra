﻿using System.Collections.Generic;

namespace Tools
{
    [System.Serializable]
    public class LinkList<L> : List<L> 
        where L : Link
    {
        public bool Contains(Node a, Node b)
        {
            foreach (var l in this) if (l.Contains(a, b)) return true;
            return false;
        }
        
        public Link GetLink(Node a, Node b)
        {
            Link pathLink = null;
            foreach (var l in this)
            {
                if (l.Contains(a, b))
                {
                    pathLink = l;
                    break;
                }
            }
            
            return pathLink;
        }

        public bool TryGetLink(Node a, Node b, out L pathLink)
        {
            foreach (var l in this)
            {
                if (l.Contains(a, b))
                {
                    pathLink = l;
                    return true;
                }
            }

            pathLink = null;
            return false;
        }
    }
}