﻿using Tools;

namespace Pathfinding
{
    [System.Serializable]
    public class DijkstraPath : Path
    {
        public DijkstraPath(Node[] nodes) : base(nodes)
        {
        }
    }
}