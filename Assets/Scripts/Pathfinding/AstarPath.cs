﻿using UnityEngine;
using Tools;

namespace Pathfinding
{
    [System.Serializable]
    public class AstarPath : Path
    {
        [SerializeField] private int[] m_Costs;
        [SerializeField] private float[] m_Heuristics;

        public AstarPath(Node[] nodes, int[] costs, float[] heuristics) 
            : base(nodes)
        {
            m_Costs = costs;
            m_Heuristics = heuristics;
        }
    }
}