using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tools;
using UnityEngine;

namespace Pathfinding
{
    public struct AstarNode
    {
        public int Cost;
        public float Heuristic;
    }

    public class Astar
    {
        private static bool Contains(List<KeyValuePair<Node, AstarNode>> list, Node node)
        {
            foreach (var astarNode in list)
            {
                if (astarNode.Key == node)
                {
                    return true;
                }
            }

            return false;
        }


        public static void DebugSolution(ref Path shortestPath)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"Shortest path : \n");
            for (int i = 0; i < shortestPath.Nodes.Count; i++)
            {
                sb.Append($"{shortestPath.Nodes[i].gameObject.name}, \n");
            }

            Debug.Log(sb.ToString());
        }
        
        public static int Compare2Node(KeyValuePair<Node, AstarNode> a, KeyValuePair<Node, AstarNode> b)
        {
            if (a.Value.Heuristic < b.Value.Heuristic)
            {
                return 1;
            }
            else if (Math.Abs(a.Value.Heuristic - b.Value.Heuristic) < 0.00001f)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }
        
        public static void UpdateDistances(KeyValuePair<Node, AstarNode> current, KeyValuePair<Node, AstarNode> end,
            ref List<KeyValuePair<Node, AstarNode>> astarNodes, 
            ref List<KeyValuePair<Node, AstarNode>> openPath, ref List<KeyValuePair<Node, AstarNode>> closedPath, 
            Func<Node, Node, bool> distConditions)
        {
            // loop through all the neighbor to update the distance from the node
            // and set the previous of the neighbor to be the current node 
            foreach (var neighbor in current.Key.Neighbors)
            {
                if (!(Contains(closedPath, neighbor) || Contains(openPath, neighbor)) && distConditions?.Invoke(current.Key, neighbor) == true)
                {
                    Node key = astarNodes[neighbor.GridIndex].Key;
                    AstarNode value = astarNodes[neighbor.GridIndex].Value;

                    value.Cost = current.Value.Cost + 1;
                    value.Heuristic = value.Cost + Vector3.Distance(neighbor.transform.position, end.Key.transform.position);

                    astarNodes[neighbor.GridIndex] = new KeyValuePair<Node, AstarNode>(key, value);
                    openPath.Add(astarNodes[neighbor.GridIndex]);
                }

                closedPath.Add(current);
            }
        }

        public static Path GetShortestPath<N>(ref List<N> nodes, int startNodeIndex, int endNodeIndex, Func<Node, Node, bool> distConditions)
            where N : Node
        {
            var path = new List<Node>();
            var costs = new List<int>();
            var heuristics = new List<float>();
            var astarNodes = new List<KeyValuePair<Node, AstarNode>>();

            for (int i = 0; i < nodes.Count; i++)
            {
                astarNodes.Add(new KeyValuePair<Node, AstarNode>(nodes[i], new AstarNode { Cost = 0, Heuristic = 0.0f }));
            }

            // if the node are the same the shortest path is the node itself (no path)
            if (startNodeIndex == endNodeIndex)
            {
                path.Add(nodes[startNodeIndex]);
            }
            else
            {
                var closedPath = new List<KeyValuePair<Node, AstarNode>>();
                closedPath.AddRange(astarNodes);

                var openPath = new List<KeyValuePair<Node, AstarNode>>();
                openPath.AddRange(astarNodes);
                openPath.Sort(Compare2Node);

                openPath.Add(astarNodes[startNodeIndex]);

                //tant que openList n'est pas vide
                while (openPath.Count > 0)
                {
                    //    u = openList.defiler()
                    var current = openPath[0];

                    // when we go on the endNode, we loop backward to get the path
                    if (current.Key == astarNodes[endNodeIndex].Key)
                    {
                        //reconstituerChemin(u)
                        while (openPath.Contains(current))
                        {
                            path.Insert(0, current.Key);
                            costs.Insert(0, current.Value.Cost);
                            heuristics.Insert(0, current.Value.Heuristic);
                            current = openPath[openPath.IndexOf(current)];
                        }

                        // add the starting node to the path
                        path.Insert(0, current.Key);
                        costs.Insert(0, current.Value.Cost);
                        heuristics.Insert(0, current.Value.Heuristic);
                        break;
                    }

                    UpdateDistances(current, astarNodes[endNodeIndex], 
                        ref astarNodes, ref openPath, ref closedPath, distConditions);
                }
            }

            return new AstarPath(path.ToArray(), costs.ToArray(), heuristics.ToArray());
        }
    }
}