﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gameplay;
using Tools;
using UnityEngine;

namespace Pathfinding
{
    public enum Pathfinder : Int32
    {
        Dijkstra = 0, 
        Astar = 1
    }

    public class PathNodeGrid : NodeGrid<PathNode, PathLink>
    {
        [SerializeField] private Transform m_Transform;

        [field: Header("GRID")]

        [field: SerializeField] public GameObject PrefabNode { get; private set; }
        [field: SerializeField] public Vector2Int Dimension { get; private set; }
        [field: SerializeField] public Vector2 OffsetSize { get; private set; }

        [field: SerializeField] [field: Range(0.1f, 1.0f)] 
        public float LinksWidth { get; private set; } = .25f;

        [field: SerializeField] public Material LineRendererMaterial { get; private set; }
        [field: SerializeField] public Gradient ColorLinkHiding { get; private set; }
        [field: SerializeField] public Gradient ColorLinkRunning { get; private set; }
        [field: SerializeField] public Gradient ColorLinkRebooting { get; private set; }
        [field: SerializeField] public Gradient ColorLinkDown { get; private set; }

        #region Unity
        
        private void Update()
        {
            foreach (var link in Links)
            {
                switch (link.Status)
                {
                    case LinkStatus.Running:
                        link.RebootingTime = 0f;
                        link.Gradient = ColorLinkRunning;
                        
                        if (link.IsHiding && link.LineRenderer != null)
                        {
                            link.LineRenderer.colorGradient = ColorLinkHiding;
                        }
                        break;
                    case LinkStatus.Down:
                        link.RebootingTime = 0f;
                        link.Gradient = ColorLinkDown;
                        break;
                    case LinkStatus.Reboot:
                        link.RebootingTime += Time.fixedDeltaTime;
                        link.Gradient = ColorLinkRebooting;

                        if (link.RebootingTime >= PathfindingManager.Instance?.MaxRebootTime)
                        {
                            link.Status = LinkStatus.Running;
                            link.RebootingTime = 0f;
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private void OnDrawGizmos()
        {
            if (m_Nodes.Count > 0)
            {
                for (int i = 0; i < m_Nodes.Count; i++)
                {
                    if (m_Nodes[i] != null)
                    {
                        UnityEditor.Handles.Label(
                            (Vector3.up * 2f) + m_Nodes[i].transform.position,
                            $"I: {i}");
                    }
                }
            }
        }

        #endregion
        
        /// <summary>
        /// Initialize the grid by instantiating all the node object
        /// and set their neighbors
        /// </summary>
        public override void Initialize()
        {
            m_Links = new LinkList<PathLink>();
            m_Nodes = new List<PathNode>(Dimension.x * Dimension.y);
            
            for (int y = 0, i = 0; y < Dimension.y; y++)
            {
                for (int x = 0; x < Dimension.x; x++, i++)
                {
                    var prefab = GameObject.Instantiate(PrefabNode, m_NodesContainer.transform);

                    if (!prefab.TryGetComponent(out PathNode node))
                    {
                        Debug.LogWarning($"Node ({i}) doesn't have a NodeComponent..");
                    }
                    else
                    {
                        Vector3 offset = new Vector3(
                            x * OffsetSize.x, 
                            Mathf.Atan2(y, x),
                            y * OffsetSize.y);
                        
                        node.GridIndex = i;
                        node.transform.position = m_Transform.position + offset;
                        node.name = $"Node ({i})";
                        m_Nodes.Add(node);
                    }
                }
            }

            
            for (int i = 0; i < Dimension.x * Dimension.y; i++) SetSquareSurroundingNeighbors(i);
        }
        
        protected override void CreateLink(int nodeIndex, int neighborIndex)
        {
            GameObject go = new GameObject($"Link ({nodeIndex} - {neighborIndex})", typeof(LineRenderer));
            go.transform.parent = m_LinksContainer;

            if (go.TryGetComponent(out LineRenderer lineRenderer))
            {
                lineRenderer.material = LineRendererMaterial;
                lineRenderer.SetPosition(0, m_Nodes[nodeIndex].transform.position);
                lineRenderer.SetPosition(1, m_Nodes[neighborIndex].transform.position);
                lineRenderer.widthCurve = AnimationCurve.Constant(0, 1, LinksWidth);

                Links.Add(new PathLink()
                {
                    A = m_Nodes[nodeIndex],
                    B = m_Nodes[neighborIndex],
                    Status = LinkStatus.Running,
                    LineRenderer = lineRenderer
                });
            }
        }

        protected override void DestroyLink(PathLink link)
        {
            Destroy(link.LineRenderer.gameObject);
        }

        /// <summary>
        /// Set the 8 surrounding neighbors of a point (square)
        /// </summary>
        /// <param name="i">Node index</param>
        private void SetSquareSurroundingNeighbors(int i)
        {
            int R = Dimension.x;
            int C = Dimension.y;
            int Max = R * C;

            int East = (i + 1) - ((i % R) / (R - 1)) * R;
            int West = (i - 1) + (((i + (R - 1)) % R) / (R - 1)) * R;
            int North = (i + R) - ((i % Max) / (Max - 1)) * Max;
            int South = (i - R) + (((i + (Max - 1)) % Max) / (Max - 1)) * Max;
            
            int I = i + 1;

            // The north can't exceed the max value of the grid R * C
            // Also check if North as been well calculated 
            if ((North - R) == i && North < Max)
            {
                // Test if west + north is not before the left limit of the grid
                if (West + R < Max && i % R != 0)
                {
                    AddLink(i, West + R);
                }
                
                AddLink(i, North);

                // Test if east + north is not after the right limit of the grid
                if (East + R < Max && I % R != 0)
                {
                    AddLink(i, East + R);
                }
            }
            
            // Test if west is not before the left limit of the grid
            if (West < Max && i % R != 0)
            {
                AddLink(i, West);
            }

            // Test if east is not after the right limit of the grid
            if (East < Max && I % R != 0)
            {
                AddLink(i, East);
            }

            // The south can't be under 0
            // Also check if South as been well calculated 
            if (South + R == i && South >= 0)
            {
                // Test if west - south is not before the left limit of the grid
                if (West < Max && i % R != 0)
                {
                    AddLink(i, West - R);
                }
                
                AddLink(i, South);

                // Test if east - south is not after the right limit of the grid
                if (East < Max && I % R != 0)
                {
                    AddLink(i, East - R);
                }
            }
        }
        
        /// <summary>
        /// Use a pathfinding algorythm to find the shortest path from A to B
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        /// <returns></returns>
        public Path ABSearch(int startIndex, int endIndex, Pathfinder pathfinder = Pathfinder.Dijkstra)
        {
            Path path = null;

            switch (pathfinder)
            {
                case Pathfinder.Dijkstra:
                    path = Dijkstra.GetShortestPath(ref m_Nodes, startIndex, endIndex, (current, neighbor) =>
                    {
                        LinkStatus linkStatus = LinkStatus.Running;
                        if (m_Links.TryGetLink(current, neighbor, out PathLink link) && link != null && link.Status != linkStatus)
                        {
                            linkStatus = link.Status;
                        }

                        return linkStatus == LinkStatus.Running && ((PathNode)neighbor).Status != NodeStatus.Closed;
                    });
                    break;
                case Pathfinder.Astar:
                    path = Astar.GetShortestPath(ref m_Nodes, startIndex, endIndex, (current, neighbor) =>
                    {
                        LinkStatus linkStatus = LinkStatus.Running;
                        if (m_Links.TryGetLink(current, neighbor, out PathLink link) && link != null && link.Status != linkStatus)
                        {
                            linkStatus = link.Status;
                        }

                        return linkStatus == LinkStatus.Running && ((PathNode)neighbor).Status != NodeStatus.Closed;
                    });
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(pathfinder), pathfinder, null);
            }

            return path;
        }
    }
}