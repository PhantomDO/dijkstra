﻿using System.Collections.Generic;
using Tools;
using UnityEngine;

namespace Pathfinding
{
    [System.Serializable]
    public enum LinkStatus
    {
        Running, Down, Reboot
    }

    [System.Serializable]
    public class PathLink : Link
    {
        public bool IsHiding;
        public LinkStatus Status;
        public LineRenderer LineRenderer;
        public float RebootingTime;

        private Gradient m_Gradient;
        public Gradient Gradient
        {
            get => m_Gradient;
            set
            {
                m_Gradient = value;
                if (LineRenderer != null)
                {
                    LineRenderer.colorGradient = m_Gradient;
                } 
            }
        }
    }
}