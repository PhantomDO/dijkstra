﻿using System;
using System.Collections;
using Pathfinding;
using Tools;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Gameplay
{
    public class NodeSelection : MonoBehaviour
    {
        [SerializeField] private PathNodeGrid m_PathNodeGrid;
        [SerializeField] private Color m_ColorNodeLinkSelectedAdd;
        [SerializeField] private Color m_ColorNodeLinkSelectedRemove;
        
        private int m_ClickMouseLeft, m_ClickMouseRight;
        private int[] m_NodesSelectedLeftClick, m_NodesSelectedRightClick;
        
        #region Input
        
        public void LeftClick(InputAction.CallbackContext context)
        {
            if (Mouse.current.leftButton.isPressed)
            {
                m_ClickMouseLeft++;
                SelectNode(ref m_NodesSelectedLeftClick, ref m_ClickMouseLeft, 
                    m_ColorNodeLinkSelectedAdd, (indexA, indexB, init) =>
                    {
                        if (m_PathNodeGrid && PathfindingManager.Instance)
                        {
                            PathLink pathLinkFounded = null;
                            var pGrid = m_PathNodeGrid;
                            foreach (var link in pGrid.Links)
                            {
                                if (link.Contains(pGrid.Nodes[indexA], pGrid.Nodes[indexB]))
                                {
                                    pathLinkFounded = link;
                                    break;
                                }
                            }

                            if (pathLinkFounded != null)
                            {
                                if (pathLinkFounded.Status == LinkStatus.Down)
                                {
                                    pathLinkFounded.Status = LinkStatus.Reboot;
                                    Debug.LogWarning($"Rebooting the {pathLinkFounded.LineRenderer.gameObject.name}, Status : {pathLinkFounded.Status}");
                                }
                            }
                        }
                    });
            }
        }
        
        public void RightClick(InputAction.CallbackContext context)
        {
            if (Mouse.current.rightButton.isPressed)
            {
                m_ClickMouseRight++;
                SelectNode(ref m_NodesSelectedRightClick, ref m_ClickMouseRight, 
                    m_ColorNodeLinkSelectedRemove, (indexA, indexB, init) =>
                    {
                        if (m_PathNodeGrid && PathfindingManager.Instance)
                        {
                            PathLink pathLinkFounded = null;
                            var pGrid = m_PathNodeGrid;
                            foreach (var link in pGrid.Links)
                            {
                                if (link.Contains(pGrid.Nodes[indexA], pGrid.Nodes[indexB]))
                                {
                                    pathLinkFounded = link;
                                    break;
                                }
                            }

                            if (pathLinkFounded != null)
                            {
                                if (pathLinkFounded.Status != LinkStatus.Down)
                                {
                                    pathLinkFounded.Status = LinkStatus.Down;
                                    Debug.LogWarning($"Down the {pathLinkFounded.LineRenderer.gameObject.name}, Status : {pathLinkFounded.Status}");
                                }
                            }
                        }
                    });
            }
        }

        #endregion

        #region Actions
        
        public int SelectGridNode()
        {
            int n = -1;
            float distance = Mathf.Infinity;

            if (m_PathNodeGrid != null)
            {
                for (int i = 0; i < m_PathNodeGrid.Nodes.Count; i++)
                {
                    var node = m_PathNodeGrid.Nodes[i];
                    Vector3 worldPosition = Vector3.zero;
                    Plane plane = new Plane(Vector3.up, 0);
                    Ray ray = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());

                    if (plane.Raycast(ray, out float dist))
                    {
                        worldPosition = ray.GetPoint(dist);
                    }

                    float d = Vector3.Distance(worldPosition, node.transform.position);
                    if (d < distance)
                    {
                        distance = d;
                        n = i;
                    }
                }
            }

            return n;
        }
        
        private void SelectNode(ref int[] selectedNodes, ref int clicks, Color colorAction, Action<int, int, bool> linkAction)
        {
            if (clicks == 1)
            {
                selectedNodes = new int[2];
                ChangeSelectedNodeColor(ref selectedNodes, 0, colorAction);
            }
            else if (clicks == 2)
            {
                ChangeSelectedNodeColor(ref selectedNodes, 1, colorAction);
                linkAction?.Invoke(selectedNodes[0], selectedNodes[1], false);
                StartCoroutine(FadeBackToBaseColor(.1f, selectedNodes, Color.white));
                clicks = 0;
            }
        }

        private void ChangeSelectedNodeColor(ref int[] array, int index, Color color)
        {
            array[index] = SelectGridNode();
            var node = m_PathNodeGrid.Nodes[array[index]];
            if (node != null && node.TryGetComponent(out MeshRenderer mr) && mr != null)
            {
                mr.material.color = color;
            }
        }
        
        private IEnumerator FadeBackToBaseColor(float time, int[] nodes, Color baseColor)
        {
            if (m_PathNodeGrid != null)
            {
                yield return new WaitForSeconds(time);

                foreach (var node in nodes)
                {
                    if (m_PathNodeGrid.Nodes[node].TryGetComponent(out MeshRenderer mr) && mr != null)
                    {
                        mr.material.color = baseColor;
                    }
                }
            }

            yield break;
        }

        #endregion
    }
}