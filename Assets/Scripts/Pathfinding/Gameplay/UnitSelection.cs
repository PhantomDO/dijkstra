﻿using System;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Gameplay
{
    /// <summary>
    /// https://www.habrador.com/tutorials/select-units-within-rectangle/
    /// </summary>
    public class UnitSelection : MonoBehaviour
    {
        [Header("Unit Selection")] 
        [SerializeField] private List<PathUnit> m_SelectedUnits = new List<PathUnit>();
        [SerializeField] private List<PathUnit> m_HighlightedUnits = new List<PathUnit>();
        [SerializeField] private float m_DelaySelection = 0.3f;
        [SerializeField] private string m_UnitTag;
        [SerializeField] private Material m_MaterialDefault, m_MaterialSelected, m_MaterialHighlight;
        [SerializeField] private RectTransform m_SelectionRect;
        
        private float m_ClickTime;

        private Vector3 m_RectStartPosition, m_RectEndPosition;
        private Vector3 m_TL, m_TR, m_BL, m_BR;
        
        private Plane m_GroundPlane;

        private bool m_IsHoldingDown, m_IsHovering;
        private bool m_HasClicked, m_HasReleased;

        private PathUnit _mPreviousHighlightedPathUnit;

        #region Unity

        private void Start()
        {
            m_SelectionRect.gameObject.SetActive(false);
            m_GroundPlane = new Plane(Vector3.up, Vector3.zero);
        }

        private void Update()
        {
            CheckMouseInputs();
            SelectUnits();
        }

        #endregion

        #region InputAction
        
        public void CheckMouseInputs()
        {
            m_HasClicked = m_HasReleased = m_IsHoldingDown = m_IsHovering = false;

            if (Mouse.current.leftButton.wasPressedThisFrame)
            {
                m_ClickTime = Time.time;
                m_RectStartPosition = Mouse.current.position.ReadValue();
                Debug.Log($"Mouse click");
            }
            else if (Mouse.current.leftButton.wasReleasedThisFrame)
            {
                if (Time.time - m_ClickTime <= m_DelaySelection)
                {
                    m_HasClicked = true;
                }

                m_HasReleased = true;
                Debug.Log($"Mouse released");
            }
            else if (Mouse.current.leftButton.isPressed)
            {
                if (Time.time - m_ClickTime > m_DelaySelection)
                {
                    m_IsHoldingDown = true;
                }
                Debug.Log($"Mouse press");
            }
            else
            {
                m_IsHovering = true;
            }
        }
        
        #endregion
        
        #region Selector
        
        /// <summary>
        /// Is a point within a triangle
        /// From http://totologic.blogspot.se/2014/01/accurate-point-in-triangle-test.html
        /// </summary>
        /// <param name="p"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <returns></returns>
        bool IsWithinTriangle(Vector3 p, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            //Need to set z -> y because of other coordinate system
            float denominator = ((p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z));

            float a = ((p2.z - p3.z) * (p.x - p3.x) + (p3.x - p2.x) * (p.z - p3.z)) / denominator;
            float b = ((p3.z - p1.z) * (p.x - p3.x) + (p1.x - p3.x) * (p.z - p3.z)) / denominator;
            float c = 1 - a - b;

            //The point is within the triangle if 0 <= a <= 1 and 0 <= b <= 1 and 0 <= c <= 1
            return (a >= 0f && a <= 1f) && 
                   (b >= 0f && b <= 1f) && 
                   (c >= 0f && c <= 1f);
        }

        /// <summary>
        /// Is a unit within a polygon determined by 4 corners
        /// The polygon forms 2 triangles, so we need to check if a point is within any of the triangles
        /// Triangle 1: TL - BL - TR
        /// Triangle 2: TR - BL - BR
        /// </summary>
        /// <param name="unitPos"></param>
        /// <returns></returns>
        private bool IsInsidePolygon(Vector3 unitPos)
        {
            return IsWithinTriangle(unitPos, m_TL, m_BL, m_TR) ||
                   IsWithinTriangle(unitPos, m_TR, m_BL, m_BR);
        }
        
        #endregion

        public void SelectUnits()
        {
            if (m_HasClicked)
            {
                foreach (var unit in m_SelectedUnits)
                {
                    unit.MeshRenderer.material = m_MaterialDefault;
                }

                m_SelectedUnits.Clear();
                
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue()), out RaycastHit hit, 200f))
                {
                    if (hit.collider.CompareTag(m_UnitTag))
                    {
                        if (hit.collider.gameObject.TryGetComponent(out PathUnit unit))
                        {
                            // Change material
                            unit.MeshRenderer.material = m_MaterialSelected;
                            m_SelectedUnits.Add(unit);
                        }
                    }
                }
            }

            if (m_IsHoldingDown)
            {
                // display rect
                DisplayRect();

                m_HighlightedUnits.Clear();

                if (PathfindingManager.Instance != null)
                {
                    for (int i = 0; i < PathfindingManager.Instance.Units.Length; i++)
                    {
                        var unit = PathfindingManager.Instance.Units[i];

                        if (unit.MeshRenderer != null)
                        {
                            if (IsInsidePolygon(unit.transform.position))
                            {
                                // Change material or shader around unit
                                unit.MeshRenderer.material = m_MaterialHighlight;
                                m_HighlightedUnits.Add(unit);
                            }
                            else
                            {
                                // Change material or shader to normal
                                unit.MeshRenderer.material = m_MaterialDefault;
                            }
                        }
                    }
                }
            }

            if (m_HasReleased)
            {
                m_SelectionRect.gameObject.SetActive(false);

                if (m_HighlightedUnits.Count > 0)
                {
                    m_SelectedUnits.Clear();

                    foreach (var unit in m_HighlightedUnits)
                    {
                        if (unit.MeshRenderer != null)
                        {
                            unit.MeshRenderer.material = m_MaterialSelected;
                            m_SelectedUnits.Add(unit);
                        }
                    }

                    m_HighlightedUnits.Clear();
                }
            }

            ResetHighlightedUnits();

            if (m_IsHovering)
            {
                HighlightUnits();
            }
        }

        public void ResetHighlightedUnits()
        {
            if (_mPreviousHighlightedPathUnit != null)
            {
                if (!m_SelectedUnits.Contains(_mPreviousHighlightedPathUnit) && _mPreviousHighlightedPathUnit.MeshRenderer != null)
                {
                    _mPreviousHighlightedPathUnit.MeshRenderer.material = m_MaterialDefault;
                    _mPreviousHighlightedPathUnit = null;
                }
            }
        }
        
        public void HighlightUnits()
        {
            //Fire a ray from the mouse position to get the unit we want to highlight  
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue()), out RaycastHit hit, 200f))
            {
                if (hit.collider.CompareTag(m_UnitTag) && hit.collider.gameObject.TryGetComponent(out PathUnit unit))
                {
                    if (!m_SelectedUnits.Contains(unit) && unit.MeshRenderer != null)
                    {
                        unit.MeshRenderer.material = m_MaterialHighlight;
                        _mPreviousHighlightedPathUnit = unit;
                    }
                }
            }
        }

        private void DisplayRect()
        {
            if (!m_SelectionRect.gameObject.activeInHierarchy)
            {
                m_SelectionRect.gameObject.SetActive(true);
            }

            // create rect
            m_RectEndPosition = Mouse.current.position.ReadValue();

            Vector3 mid = (m_RectStartPosition + m_RectEndPosition) / 2f;

            m_SelectionRect.position = mid;

            Vector2 size = new Vector2(
                Mathf.Abs(m_RectStartPosition.x - m_RectEndPosition.x),
                Mathf.Abs(m_RectStartPosition.x - m_RectEndPosition.x));

            m_SelectionRect.sizeDelta = size;

            m_TL = new Vector3(mid.x - (size.x * 0.5f), mid.y + (size.y * 0.5f), 0f);
            m_TR = new Vector3(mid.x + (size.x * 0.5f), mid.y + (size.y * 0.5f), 0f);
            m_BL = new Vector3(mid.x - (size.x * 0.5f), mid.y - (size.y * 0.5f), 0f);
            m_BR = new Vector3(mid.x + (size.x * 0.5f), mid.y - (size.y * 0.5f), 0f);

            Ray rayTL = Camera.main.ScreenPointToRay(m_TL);
            Ray rayTR = Camera.main.ScreenPointToRay(m_TR);
            Ray rayBL = Camera.main.ScreenPointToRay(m_BL);
            Ray rayBR = Camera.main.ScreenPointToRay(m_BR);

            //From screen to world
            float distanceToPlane = 0f;
            //Fire ray from camera
            if (m_GroundPlane.Raycast(rayTL, out distanceToPlane))
            {
                m_TL = rayTL.GetPoint(distanceToPlane);
            }
            if (m_GroundPlane.Raycast(rayTR, out distanceToPlane))
            {
                m_TR = rayTR.GetPoint(distanceToPlane);
            }
            if (m_GroundPlane.Raycast(rayBL, out distanceToPlane))
            {
                m_BL = rayBL.GetPoint(distanceToPlane);
            }
            if (m_GroundPlane.Raycast(rayBR, out distanceToPlane))
            {
                m_BR = rayBR.GetPoint(distanceToPlane);
            }
        }
    }
}