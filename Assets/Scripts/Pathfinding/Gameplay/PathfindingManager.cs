﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using TMPro;
using Tools;
using UnityEngine.InputSystem;

namespace Gameplay
{
    public class PathfindingManager : MonoSingleton<PathfindingManager>
    {
        [SerializeField] private Pathfinder m_Pathfinder = Pathfinder.Dijkstra;
        [SerializeField] private PathNodeGrid m_PathNodeGrid;

        [SerializeField] private float m_MaxRebootTime = 2f;
        public float MaxRebootTime => m_MaxRebootTime;

        [SerializeField] private int m_UnitCount;
        [SerializeField] private GameObject m_UnitPrefab;
        [SerializeField] private Transform m_UnitParent;
        [SerializeField] private PathUnit[] m_Units;
        public PathUnit[] Units => m_Units;

        [Header("Grid colors")]
        [SerializeField] private Gradient m_ColorPath;
        [SerializeField] private Gradient m_ColorPathDefault;
        [SerializeField] private Color m_ColorNodeStart;
        [SerializeField] private Color m_ColorNodeEnd;

        private bool m_HideLinks;

        #region Unity
        
        private void Start()
        {
            // Initialize unit
            if (m_PathNodeGrid != null)
            {
                var pGrid = m_PathNodeGrid;
                pGrid.DestroyGrid();
                pGrid.Initialize();

                CreateUnits();
                RandomPathForAllUnits();

                m_HideLinks = true;
                ShowLinks();
            }
        }

        private void Update()
        {
            if (Keyboard.current.dKey.wasPressedThisFrame)
            {
                m_HideLinks = !m_HideLinks;
                ShowLinks();
            }
        }

        private void FixedUpdate()
        {
            // Reset the path for each unit who ended moving
            foreach (var unit in m_Units)
            {
                if (m_PathNodeGrid && unit.IndexCurrentPath > 0 && unit.IndexCurrentPath < unit.Path.Nodes.Count)
                {
                    var current = (PathNode)unit.Path.Nodes[unit.IndexCurrentPath - 1];
                    var next = (PathNode)unit.Path.Nodes[unit.IndexCurrentPath];
                    if (next.Status == NodeStatus.Closed || 
                        (m_PathNodeGrid.Links.TryGetLink(current, next, out PathLink link) && link?.Status != LinkStatus.Running)) 
                    {
                        Debug.Log($"{unit.name} changed his path");
                        ((PathNode)unit.Path.Start).SetColor(m_ColorNodeStart, true);
                        CreateUnitPath(unit, current.GridIndex, ((PathNode)unit.Path.End).GridIndex);
                    }
                }

                if (unit.IndexCurrentPath > -1 && unit.IndexCurrentPath >= unit.Path.Nodes.Count)
                {
                    unit.LineRenderer.colorGradient = m_ColorPathDefault;
                    ((PathNode)unit.Path.Start).SetColor(m_ColorNodeStart, true);
                    ((PathNode)unit.Path.End).SetColor(m_ColorNodeEnd, true);
                    unit.Path.SetColor(m_ColorPathDefault, true);
                    unit.SetPath(null);
                }
            }
        }

        #endregion
        
        #region Actions
        
        public void OnPathfinderChanged(TMP_Dropdown dropdown)
        {
            m_Pathfinder = (Pathfinder)dropdown.value;
        }

        public void RandomPathForAllUnits()
        {
            if (m_PathNodeGrid != null)
            {
                foreach (var unit in m_Units)
                {
                    CreateUnitPath(unit, m_PathNodeGrid.GetIndexAtPosition(unit.transform.position), 
                        UnityEngine.Random.Range(0, m_PathNodeGrid.Nodes.Count));
                }
            }
        }

        #endregion

        public void ShowLinks()
        {
            if (m_PathNodeGrid)
            {
                foreach (var link in m_PathNodeGrid.Links)
                {
                    link.IsHiding = m_HideLinks;
                }
            }
        }

        public void CreateUnits()
        {
            if (m_PathNodeGrid != null)
            {
                // Initialize unit
                m_Units = new PathUnit[m_UnitCount];

                List<int> rdmIndexs = new List<int>();

                for (int i = 0; i < m_UnitCount; i++)
                {
                    var prefab = GameObject.Instantiate(m_UnitPrefab, m_UnitParent);
                    prefab.name = $"Unit ({i})";
                    if (prefab.TryGetComponent(out PathUnit unit))
                    {
                        m_Units[i] = unit;
                        int rdmIndex = UnityEngine.Random.Range(0, m_PathNodeGrid.Nodes.Count);
                        m_Units[i].transform.position = m_PathNodeGrid.Nodes[rdmIndex].transform.position;
                    }
                }
            }
        }

        public void CreateUnitPath(PathUnit pathUnit, int startNodeIndex, int endNodeIndex)
        {
            if (m_PathNodeGrid != null)
            {
                pathUnit.LineRenderer.colorGradient = m_ColorPath;

                pathUnit.SetPath(m_PathNodeGrid.ABSearch(startNodeIndex, endNodeIndex));
                ((PathNode)pathUnit.Path.Start).SetColor(m_ColorNodeStart, false);
                ((PathNode)pathUnit.Path.End).SetColor(m_ColorNodeEnd, false);
                pathUnit.Path.SetColor(m_ColorPath, false);
                pathUnit.StartPath(0.1f);
            }
        }
    }
}