﻿using System.Collections;
using Tools;
using Pathfinding;
using UnityEngine;

namespace Gameplay
{
    public class PathUnit : Unit
    {
        [SerializeField] protected float m_MoveSpeed;

        [SerializeField] protected Path m_Path;
        public Path Path => m_Path;

        [SerializeField] protected int m_IndexCurrentPath;
        public int IndexCurrentPath => m_IndexCurrentPath;
        
        public Rigidbody Rigidbody { get; protected set; }
        public LineRenderer LineRenderer { get; protected set; }
        public MeshRenderer MeshRenderer { get; protected set; }

        public Vector2 Position2D => new Vector2(transform.position.x, transform.position.z);

        #region Unity
        
        protected virtual void Awake()
        {
            if (TryGetComponent(out Rigidbody rb))
            {
                Rigidbody = rb;
            }

            if (TryGetComponent(out LineRenderer lr))
            {
                LineRenderer = lr;
            }

            if (TryGetComponent(out MeshRenderer mr))
            {
                MeshRenderer = mr;
            }
        }

        protected virtual void FixedUpdate()
        {
            if (Path != null && Path.Nodes.Count > 0 && (IndexCurrentPath < Path.Nodes.Count && IndexCurrentPath >= 0))
            {
                Vector3 direction = ((PathNode)Path.Nodes[IndexCurrentPath]).transform.position - transform.position;
                Vector3 position = transform.position + (direction.normalized * m_MoveSpeed * Time.deltaTime);

                if (position.sqrMagnitude > 0.0f)
                {
                    Rigidbody.MovePosition(position);
                }
            }
        }

        #endregion

        #region Path
        
        public void SetPath(Path path)
        {
            m_Path = path;
            m_IndexCurrentPath = m_Path != null ? 0 : -1;

            if (m_Path != null && m_Path.Nodes.Count > 0)
            {
                LineRenderer.positionCount = m_Path.Nodes.Count;
                // Set lineRenderer params
                for (int i = 0; i < m_Path.Nodes.Count; i++)
                {
                    var node = (PathNode)m_Path.Nodes[i];
                    LineRenderer.SetPosition(i, new Vector3(node.Position2D.x, transform.position.y + 1, node.Position2D.y));
                }
            }
        }

        public void StartPath(float minDist)
        {
            StartCoroutine(FollowPath(minDist));
        }

        protected IEnumerator FollowPath(float minDist)
        {
            while (IndexCurrentPath != -1 && IndexCurrentPath < Path.Nodes.Count)
            {
                if (Vector2.Distance(Position2D, ((PathNode)Path.Nodes[IndexCurrentPath]).Position2D) <= minDist)
                {
                    m_IndexCurrentPath++;
                }
                yield return null;
            }

            yield break;
        }
        
        #endregion
        
    }
}