using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tools;
using UnityEngine;

namespace Pathfinding
{
    public class Dijkstra
    {
        public static void DebugSolution(ref Path shortestPath)
        { 
            StringBuilder sb = new StringBuilder();
            sb.Append($"Shortest path : \n");
            for (int i = 0; i < shortestPath.Nodes.Count; i++)
            {
                sb.Append($"{shortestPath.Nodes[i].gameObject.name}, \n");
            }

            Debug.Log(sb.ToString());
        }
        
        public static void UpdateDistances(ref Node current, ref Dictionary<Node, Node> previous, ref Dictionary<Node, float> distances, Func<Node, Node, bool> distConditions)
        {
            // loop through all the neighbor to update the distance from the node
            // and set the previous of the neighbor to be the current node 
            foreach (var neighbor in current.Neighbors)
            {
                float distance = Vector3.Distance(current.transform.position, neighbor.transform.position);
                float distanceStartToCurrent = distances[current] + distance;

                if (distanceStartToCurrent < distances[neighbor] && distConditions?.Invoke(current, neighbor) == true)
                {
                    distances[neighbor] = distanceStartToCurrent;
                    previous[neighbor] = current;
                }
            }
        }

        /// <summary>
        /// https://github.com/hasanbayatme/unity-dijkstras-pathfinding/blob/master/Assets/Scripts/Graph.cs
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="links"></param>
        /// <param name="startNodeIndex"></param>
        /// <param name="endNodeIndex"></param>
        /// <returns></returns>
        public static Path GetShortestPath<N>(ref List<N> nodes, int startNodeIndex, int endNodeIndex, Func<Node, Node, bool> distConditions) 
            where N : Node
        {
            List<Node> path = new List<Node>();

            // if the node are the same the shortest path is the node itself (no path)
            if (startNodeIndex == endNodeIndex)
            {
                path.Add(nodes[startNodeIndex]);
            }
            else
            {
                var unVisited = new List<Node>();
                var previous = new Dictionary<Node, Node>();
                var distances = new Dictionary<Node, float>();
                // Add all the node in the unVisited list, to remove it after we went on it
                // Also set all the distances of each path at maxValue
                foreach (var node in nodes)
                {
                    unVisited.Add(node);
                    distances.Add(node, float.MaxValue);
                }

                // the starting node is set at 0 
                distances[nodes[startNodeIndex]] = 0f;

                // loop through all the node until the list isn't empty
                while (unVisited.Count > 0)
                {
                    // each loop time, order the node in the list by the distances map (min to max)
                    unVisited = unVisited.OrderBy(node => distances[node]).ToList();
                    // always get the first node and remove it from the unvisited list
                    var current = unVisited[0];
                    unVisited.Remove(current);

                    // when we go on the endNode, we loop backward to get the path
                    if (current == nodes[endNodeIndex])
                    {
                        // backward loop to construct the path by pushing front 
                        while (previous.ContainsKey(current))
                        {
                            path.Insert(0, current);
                            current = previous[current];
                        }

                        // add the starting node to the path
                        path.Insert(0, current);
                        break;
                    }

                    UpdateDistances(ref current, ref previous, ref distances, distConditions);
                }
            }

            return new DijkstraPath(path.ToArray());
        }
    }

}
