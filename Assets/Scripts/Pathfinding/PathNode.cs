﻿using System.Collections.Generic;
using Tools;
using UnityEngine;

namespace Pathfinding
{
    public enum NodeStatus
    {
        Open, Closed,
    }

    public class PathNode : Node
    {
        [SerializeField] private Color m_BaseColor;
        [SerializeField] private NodeStatus m_Status;
        
        public Vector2 Position2D => new Vector2(transform.position.x, transform.position.z);
        public NodeStatus Status { get => m_Status; set => m_Status = value; }

        #region Unity

        protected override void Awake()
        {
            base.Awake();
            m_Status = NodeStatus.Open;
            if (TryGetComponent(out MeshRenderer mr) && mr != null)
            {
                m_BaseColor = mr.material.color;
            }
        }

        #endregion
        
        public void SetColor(Color color, bool reset = false)
        {
            if (TryGetComponent(out MeshRenderer mr) && mr != null)
            {
                mr.material.color = reset ? m_BaseColor : color;
            }
        }
    }
}