﻿using System.Collections.Generic;
using Tools;
using UnityEngine;

namespace Genetics
{
    public class GeneticManager : MonoSingleton<GeneticManager>
    {
        [field: SerializeField] public List<City> Destinations { get; private set; }

        public GeneticNodeGrid NodeGrid;
        public float Mutation = 0.015f;
        [Range(0.1f, 0.99f)] public float TournamentPercent = .1f;
        public bool Best = true;
        public int GenerationCount = 100;
        public Gradient BestGradient;

        private List<LineRenderer> m_LineRenderers;
        private List<Population> m_Generations;

        public void Start()
        {
            m_Generations = new List<Population>();
            m_LineRenderers = new List<LineRenderer>();
            NodeGrid.Initialize();
            for (int i = 0; i < NodeGrid.Nodes.Count; i++)
            {
                Destinations.Add(NodeGrid.Nodes[i]);
            }

            GameObject lines = new GameObject("LineRenderers");
            lines.transform.parent = gameObject.transform;

            for (int i = 0; i < GenerationCount; i++)
            {
                GameObject child = new GameObject($"LineRenderer ({i})", typeof(LineRenderer));
                child.transform.parent = lines.transform;

                if (child.TryGetComponent(out LineRenderer lr))
                {
                    Gradient gradient = new Gradient();
                    gradient.SetKeys(
                    new GradientColorKey[]
                    {
                        new GradientColorKey(
                            new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f)), 
                            i * GenerationCount)
                    }, 
                    new GradientAlphaKey[]
                    {
                        new GradientAlphaKey(1f, i * GenerationCount)
                    });

                    m_LineRenderers.Add(lr);
                    lr.startWidth = 0.1f;
                    lr.colorGradient = gradient;
                    lr.material = new Material(Shader.Find("Sprites/Default"));
                }
            }
        }

        public void DrawTrack(uint trackNumber)
        {

        }

        public void Travelling(bool init = false)
        {
            m_Generations.Clear();
            var pop = new Population(this, Destinations.Count, init);
            Debug.Log($"Start distance : {pop.GetFittest().Distance}");
            m_Generations.Add(pop);

            pop = Evolve(pop);
            m_Generations.Add(pop);

            var vecs = new List<Vector3>();
            for (int i = 0; i < GenerationCount; i++)
            {
                pop = Evolve(pop);
                m_Generations.Add(pop);
                foreach (var v in pop.GetFittest().Nodes)
                {
                    vecs.Add(v.transform.position);
                }

                m_LineRenderers[i].positionCount = vecs.Count;
                m_LineRenderers[i].SetPositions(vecs.ToArray());
                vecs.Clear();
            }

            m_Generations.Sort((a, b) =>
            {
                return a.GetFittest().Distance <= b.GetFittest().Distance 
                    ? m_Generations.IndexOf(a) : m_Generations.IndexOf(b);
            });

            pop = m_Generations[0];
            m_LineRenderers[0].colorGradient = BestGradient;
            m_LineRenderers[0].widthCurve = AnimationCurve.Constant(0, 1, 0.2f);

            Debug.Log($"End distance : {pop.GetFittest().Distance}");
            var bestPop = pop.GetFittest();
        }

        public Population Evolve(Population pop)
        {
            var newPop = new Population(this, pop.Tracks.Length, false);
            int eliteOffset = 0;

            if (Best)
            {
                newPop.Tracks[0] = pop.GetFittest();
                eliteOffset++;
            }

            for (int i = eliteOffset; i < newPop.Tracks.Length; i++)
            {
                var parent1 = Selection(pop);
                var parent2 = Selection(pop);
                var child = CrossOver(parent1, parent2);
                newPop.Tracks[i] = child;
            }

            for (int i = eliteOffset; i < newPop.Tracks.Length; i++)
            {
                Mutate(pop.Tracks[i]);
            }

            return newPop;
        }

        public TrackPath CrossOver(TrackPath parent1, TrackPath parent2)
        {
            var child = new TrackPath(this);
            var startPos = Random.Range(0, parent1.Nodes.Count - 1);
            var endPos = Random.Range(0, parent1.Nodes.Count - 1);

            for (int i = 0; i < child.Nodes.Count; i++)
            {
                if (startPos < endPos && i > startPos && i < endPos)
                {
                    child.Nodes[i] = parent1.Nodes[i];
                }
                else if (startPos > endPos && !(i < startPos && i > endPos))
                {
                    child.Nodes[i] = parent1.Nodes[i];
                }
            }
            
            for (int i = 0; i < parent2.Nodes.Count; i++)
            {
                if (!child.Nodes.Contains(parent2.Nodes[i]))
                {
                    for (int j = 0; j < child.Nodes.Count; j++)
                    {
                        if (child.Nodes[j] == null)
                        {
                            child.Nodes[j] = parent2.Nodes[i];
                            break;
                        }
                    }
                }
            }
            return child;
        }

        public void Mutate(TrackPath track)
        {
            for (int i = 0; i < track.Nodes.Count; i++)
            {
                if (Random.value < Mutation)
                {
                    var j = Random.Range(0, track.Nodes.Count - 1);
                    var city1 = track.Nodes[i];
                    var city2 = track.Nodes[j];
                    track.Nodes[j] = city1;
                    track.Nodes[i] = city2;
                }
            }
        }

        public TrackPath Selection(Population pop)
        {
            var tournamentSize = Mathf.CeilToInt(Destinations.Count * TournamentPercent);
            var tournament = new Population(this, tournamentSize, false);
            for (int i = 0; i < tournamentSize; i++)
            {
                var randomID = Random.Range(0, pop.Tracks.Length);
                tournament.Tracks[i] = pop.Tracks[randomID];
            }

            return tournament.GetFittest();
        }
    }
}