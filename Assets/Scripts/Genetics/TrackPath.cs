﻿using Tools;

namespace Genetics
{
    [System.Serializable]
    public class TrackPath : Path
    {
        private float m_Distance;
        private float m_Fitness;
        private GeneticManager m_Manager;
        
        public TrackPath(GeneticManager manager, City[] destinations = null) : base()
        {
            m_Manager = manager;

            m_Fitness = 0.0f;
            m_Distance = 0.0f;

            if (destinations != null) Nodes.AddRange(destinations);
            else for (int i = 0; i < manager.Destinations.Count; i++) Nodes.Add(null);
        }

        public void Generate()
        {
            for (int i = 0; i < m_Manager.Destinations.Count; i++)
            {
                Nodes[i] = m_Manager.Destinations[i];
                m_Fitness = 0.0f;
                m_Distance = 0.0f;
            }

            var rdm = new System.Random();
            for (int i = Nodes.Count - 1; i > 0; i--)
            {
                int index = rdm.Next(i + 1);
                (Nodes[index], Nodes[i]) = (Nodes[i], Nodes[index]);
            }
        }

        public float Fitness
        {
            get
            {
                if (m_Fitness == 0.0f) m_Fitness = 1f / Distance;
                return m_Fitness;
            }
        }

        public float Distance
        {
            get
            {
                if (m_Distance == 0.0f)
                {
                    var circuitDistance = 0.0f;
                    for (int i = 0; i < Nodes.Count; i++)
                    {
                        City origin = (City)Nodes[i];
                        City destination = (City)(i + 1 < Nodes.Count ? Nodes[i + 1] : Nodes[0]);
                        circuitDistance += origin.Distance(destination);
                        m_Distance = circuitDistance;
                    }
                }

                return m_Distance;
            }
        }
    }
}