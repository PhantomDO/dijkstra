﻿using System.Collections.Generic;

namespace Genetics
{
    [System.Serializable]
    public class Population
    {
        public TrackPath[] Tracks;

        public Population(GeneticManager manager, int size, bool init)
        {
            Tracks = new TrackPath[size];
            for (int i = 0; i < size; i++) { Tracks[i] = null; }

            if (init)
            {
                for (int i = 0; i < size; i++)
                {
                    var track = new TrackPath(manager);
                    track.Generate();
                    Tracks[i] = track;
                }
            }
        }

        public TrackPath GetFittest()
        {
            var fittest = Tracks[0];
            for (int i = 0; i < Tracks.Length; i++)
            {
                if (fittest.Fitness <= Tracks[i].Fitness)
                {
                    fittest = Tracks[i];
                }
            }

            return fittest;
        }
    }
}