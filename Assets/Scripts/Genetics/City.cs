using System.Collections;
using System.Collections.Generic;
using Tools;
using UnityEngine;

namespace Genetics
{
    public class City : Node
    {

        public float Distance(City other) => Vector3.Distance(transform.position, other.transform.position);
    }
}