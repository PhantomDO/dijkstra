﻿using System.Collections.Generic;
using Pathfinding;
using Tools;
using UnityEngine;

namespace Genetics
{
    public class GeneticNodeGrid : NodeGrid<City, Road>
    {
        public bool getFromChild;
        public uint cityCount;
        public float tinyFloat = 0.08f;
        
        public override void Awake()
        {
            base.Awake();
            if (getFromChild)
            {
                Nodes.Clear();
                if (m_NodesContainer.childCount <= 0)
                {
                    for (int i = 0; i < cityCount; i++)
                    {
                        var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                        Nodes.Add(go.AddComponent<City>());
                        go.name = $"City ({i + 1})";

                        var randomPosition = Random.insideUnitSphere * cityCount * tinyFloat;
                        go.transform.position = new Vector3(randomPosition.x, 0, randomPosition.z);
                        go.transform.parent = m_NodesContainer;
                    }
                }
                else
                {
                    foreach (Transform child in m_NodesContainer)
                    {
                        if (child.TryGetComponent(out City city))
                        {
                            Nodes.Add(city);
                        }
                    }
                }
            }
        }

        public override void Initialize()
        {
            if (m_Nodes.Count <= 1) return;

            m_Links = new LinkList<Road>();
            for (int i = 0; i < m_Nodes.Count; i++)
            {
                var rand = Random.Range(1, m_Nodes.Count - 1);
                for (int j = 0; j < rand; j++)
                {
                    var rdm = Random.Range(0, m_Nodes.Count - 1);
                    if (m_Nodes[rdm] == m_Nodes[i]) rdm = Random.Range(0, m_Nodes.Count - 1);
                        
                    AddLink(i, rdm);
                }
            }
        }

        protected override void CreateLink(int nodeIndex, int neighborIndex)
        {
            Links.Add(new Road()
            {
                A = m_Nodes[nodeIndex],
                B = m_Nodes[neighborIndex]
            });
        }

        protected override void DestroyLink(Road link)
        {
        }


    }
}