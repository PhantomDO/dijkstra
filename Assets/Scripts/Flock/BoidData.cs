using System.Text;
using UnityEngine;

namespace Flocking
{
    [System.Serializable]
    public struct BoidData
    {
        public uint Mates;

        public Vector3 Position;
        public Vector3 Velocity;
        public Vector3 Acceleration;

        public static int Size() => (sizeof(float) * 3 * 3) + sizeof(uint);

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"BoidData : {{\n");
            sb.Append($"\tMates: {Mates},\n");
            sb.Append($"\tPosition: {Position},\n");
            sb.Append($"\tAcceleration: {Acceleration},\n");
            sb.Append($"\tVelocity: {Velocity},\n}}\n");
            return sb.ToString();
        }
    }
}