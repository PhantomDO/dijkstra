﻿using StateMachine;
using UnityEngine;

namespace Flocking
{
    public class BoidMovementStackStateMachine : StackStateMachine<MovementBoidState, BoidMovementEnum>
    {
        [field: SerializeField] public Boid Actor { get; private set; }

        /// <summary>
        /// The previous state
        /// </summary>
        public new MovementBoidState PreviousState
        {
            get => (MovementBoidState)base.PreviousState; 
            protected set => base.PreviousState = value;
        }

        /// <summary>
        /// The current state
        /// </summary>
        public new MovementBoidState CurrentState
        {
            get => (MovementBoidState)base.CurrentState; 
            protected set => base.CurrentState = value;
        }

        public override void Awake()
        {
            base.Awake();
            AddOnTop((uint)DefaultState);
            if (Actor == null && TryGetComponent(out Boid boid)) Actor = boid;
        }
    }
}