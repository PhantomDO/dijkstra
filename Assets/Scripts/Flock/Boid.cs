﻿using UnityEngine;

namespace Flocking
{
    public class Boid : MonoBehaviour
    {
        public BoidData Data;
        public Transform Target;
        public BoidParameters Parameters;

        public Vector3 TargetDir;

        public BoidMovementStackStateMachine Fsm { get; private set; }

        private void Awake()
        {
            if (TryGetComponent(out BoidMovementStackStateMachine fsm))
            {
                Fsm = fsm;
            }
        }

        #if UNITY_EDITOR
        /*
        private void OnDrawGizmos()
        {
            if (Parameters)
            {
                var angle = Parameters.FoV / 2;
                var qMin = Quaternion.AngleAxis(-angle, transform.up);
                var qMax = Quaternion.AngleAxis(angle, transform.up);
                var minFovPos = transform.position + (qMin * transform.forward * Parameters.Settings.RadiusVision);
                var maxFovPos = transform.position + (qMax * transform.forward * Parameters.Settings.RadiusVision);
                
                Gizmos.color = Color.red;
                Gizmos.DrawLine(transform.position, minFovPos);
                Gizmos.color = Color.cyan;
                Gizmos.DrawLine(transform.position, maxFovPos);
                var dotMinFov = Vector3.Dot(TargetDir.normalized, (minFovPos - transform.position).normalized);
                var dotMaxFov = Vector3.Dot(TargetDir.normalized, (minFovPos - transform.position).normalized);
                
                //UnityEditor.Handles.Label(minFovPos, $"{dotMinFov}");
                //UnityEditor.Handles.Label(maxFovPos, $"{dotMaxFov}");
                
                Gizmos.color = Color.green;
                Gizmos.DrawLine(transform.position, transform.position + transform.forward.normalized * Parameters.Settings.RadiusVision);

                if (FlockManager.Instance?.Hunter != null)
                {
                    var gunDir = FlockManager.Instance.Hunter.GunDirection;
                    var dot = Vector3.Dot(TargetDir.normalized, gunDir.normalized);
                    var dotFov = Vector3.Dot((minFovPos - transform.position).normalized, gunDir.normalized);
                    UnityEditor.Handles.Label(transform.position + transform.forward * Parameters.Settings.RadiusVision, $"{dot}");
                    UnityEditor.Handles.Label(maxFovPos, $"{dotFov}");
                }
            }
        }
        */
        #endif

        private void FixedUpdate()
        {
            Fsm.Run();
        }

        public bool InFov(Vector3 other)
        {
            if (Parameters.FoV <= Mathf.Epsilon) return false;
            if (Parameters.Settings.RadiusVision <= Mathf.Epsilon) return false;

            var angle = Parameters.FoV / 2;
            var dir = (other - transform.position);
            var angleFov = Vector3.Angle(transform.forward.normalized, dir.normalized);
            
            Debug.Log($"Dot in Fov: {angle > angleFov}, in distance: {dir.sqrMagnitude < Parameters.Settings.RadiusVision * Parameters.Settings.RadiusVision}");
            
            return angle > angleFov && dir.sqrMagnitude < Parameters.Settings.RadiusVision * Parameters.Settings.RadiusVision;
        }

        public Vector3 SteerTowards(Vector3 vector)
        {
            Vector3 v = vector.normalized * Parameters.Settings.MaxSpeed - Data.Velocity;
            return Vector3.ClampMagnitude(v, Parameters.Settings.MaxForce);
        }

        public bool IsGoingToCollide()
        {
            return Physics.SphereCast(transform.position, Parameters.RadiusOverlap, (Data.Velocity / Data.Velocity.magnitude),
                out RaycastHit hit, Parameters.DistanceAvoid, Parameters.LayerOverlap);
        }

        public Vector3 ObstacleDirection()
        {
            Vector3[] rayDir = BoidHelper.directions;
            
            Ray ray;
            Vector3 direction;
            for (int i = 0; i < rayDir.Length; i++)
            {
                direction = transform.TransformDirection(rayDir[i]);
                ray = new Ray(Data.Position, direction);
                //Debug.DrawRay(ray.origin, ray.direction, Color.red, 5);

                if (!Physics.SphereCast(ray, Parameters.RadiusOverlap, Parameters.DistanceAvoid, Parameters.LayerOverlap))
                {
                    return direction;
                }
            }

            return (Data.Velocity / Data.Velocity.magnitude);
        }

        #region OLD
        
        /* Old : CPU side movement
        public void Move(System.Collections.Generic.List<Boid> boids)
        {
            Data.Mates = 0;
            Vector3 cohesion = Vector3.zero;
            Vector3 alignment = Vector3.zero;
            Vector3 separation = Vector3.zero;
            foreach (var other in boids)
            {
                if (other != this)
                {
                    var direction = (other.transform.position - transform.position);
                    var sqrMagnitude = direction.sqrMagnitude;
                    if (sqrMagnitude < (Settings.RadiusVision * Settings.RadiusVision))
                    {
                        Data.Mates++;
                        cohesion += Data.Position;
                        alignment += Data.Velocity / Data.Velocity.magnitude;

                        if (sqrMagnitude < (Settings.RadiusAvoid * Settings.RadiusAvoid))
                        {
                            separation -= direction / sqrMagnitude;
                        }
                    }
                }
            }
            
            Data.Acceleration = Vector3.zero;
            if (Target != null)
            {
                Data.Acceleration = Clamp(Target.position - transform.position) * Settings.WeightFocus;
            }

            if (Data.Mates > 0)
            {
                cohesion /= Data.Mates;
                cohesion -= transform.position;

                Data.Acceleration += Clamp(alignment) * Settings.WeightAlignment;
                Data.Acceleration += Clamp(cohesion) * Settings.WeightCohesion;
                Data.Acceleration += Clamp(separation) * Settings.WeightSeparation;
            }

            if (IsGoingToCollide)
            {
                Data.Acceleration += Clamp(ObstacleDirection()) * Settings.WeightCollision;
            }
            
            Data.Velocity += Data.Acceleration * Time.deltaTime;
            float speed = Data.Velocity.magnitude;
            Vector3 dir = Data.Velocity / speed;

            speed = Mathf.Clamp(speed, Settings.MinSpeed, Settings.MaxSpeed);
            Data.Velocity = dir * speed;
            Data.Position += Data.Velocity * Time.deltaTime;

            transform.forward = Data.Velocity / Data.Velocity.magnitude;
            transform.position = Data.Position;
        }
        */

        #endregion
    }
}