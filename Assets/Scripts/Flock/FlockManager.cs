﻿using System;
using System.Collections.Generic;
using Flocking.Gameplay;
using Tools;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

namespace Flocking
{
    public class FlockManager : MonoSingleton<FlockManager>
    {
        private const int THREAD_GROUP_SIZE = 1024;

        [Header("Settings")]
        [SerializeField] private ComputeShader m_ComputeShader;
        [SerializeField] private GameObject m_BoidPrefab;
        [SerializeField] private int m_BoidNumber;
        [SerializeField] private float m_SpawnDensity = 0.08f;
        [SerializeField] private Transform[] m_Spawners;
        [SerializeField] private Transform[] m_Targets;
        [field: SerializeField] public Hunter Hunter { get; private set; }
        [field: SerializeField] public List<Boid> Boids { get; private set; }
        
        [Header("Movement Map")]
        [SerializeField] private BoidMovementEnum[] m_BoidMovementKey;
        [SerializeField] private BoidParameters[] m_BoidMovementValue;
        private Dictionary<BoidMovementEnum, BoidParameters> m_MovementSettings;
        
        #region Unity
        
        private void Start()
        {
            Boids = new List<Boid>();
            m_MovementSettings = new Dictionary<BoidMovementEnum, BoidParameters>();

            for (int i = 0; i < m_BoidMovementKey.Length; i++)
            {
                if (m_BoidMovementValue[i] != null && !m_MovementSettings.ContainsKey(m_BoidMovementKey[i]))
                {
                    m_MovementSettings.Add(m_BoidMovementKey[i], m_BoidMovementValue[i]);
                }
            }

            Vector3 randomPosition = Vector3.forward;
            Quaternion randomRotation = Quaternion.identity;

            for (int i = 0; i < m_BoidNumber; i++)
            {
                var go = GameObject.Instantiate(m_BoidPrefab, transform);
                go.name = $"Boid ({i})";
                
                randomPosition = m_Spawners[Random.Range(0, m_Spawners.Length)].position + Random.insideUnitSphere * m_SpawnDensity * .5f;
                randomRotation = Quaternion.Euler(Vector3.forward * Random.Range(0f, 360f));
                go.transform.SetPositionAndRotation(randomPosition, randomRotation);
                
                if (go.TryGetComponent(out Boid boid))
                {
                    var randomTargetIndex = Random.Range(0, m_Targets.Length);

                    if (m_MovementSettings.ContainsKey((BoidMovementEnum)boid.Fsm.CurrentState.Get()))
                    {
                        boid.Parameters = m_MovementSettings[(BoidMovementEnum)boid.Fsm.CurrentState.Get()];
                    }

                    boid.Target = m_Targets[randomTargetIndex];
                    boid.Data = new BoidData
                    {
                        Mates = 0,
                        Acceleration = Vector3.zero,
                        Position = boid.transform.position,
                        Velocity = boid.transform.forward * ((boid.Parameters.Settings.MinSpeed + boid.Parameters.Settings.MaxSpeed) / 2)
                    };

                    Boids.Add(boid);
                }
            }
        }

        private void Update()
        {
            var targetPosition = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
            
        }

        private void FixedUpdate()
        {
            if (Boids.Count <= 0) return;
            
            Move();
        }

        #endregion
        
        public void Move()
        {
            var boidSettings = new BoidSettings[Boids.Count];
            var boidDatas = new BoidData[Boids.Count];
            for (int i = 0; i < Boids.Count; i++)
            {
                var boid = Boids[i];
                boid.Data.Acceleration = Vector3.zero;

                if (boid.Target != null && boid.TargetDir.magnitude >= Mathf.Epsilon)
                {
                    boid.Data.Acceleration = boid.SteerTowards(boid.TargetDir) * boid.Parameters.WeightTarget;
                }

                if (boid.IsGoingToCollide())
                {
                    boid.Data.Acceleration += boid.SteerTowards(boid.ObstacleDirection()) * boid.Parameters.WeightCollision;
                }

                boidDatas[i] = boid.Data;
                boidSettings[i] = boid.Parameters.Settings;
            }

            var boidBuffer = new ComputeBuffer(Boids.Count, BoidData.Size());
            boidBuffer.SetData(boidDatas);

            var settingsBuffer = new ComputeBuffer(Boids.Count, BoidSettings.Size());
            settingsBuffer.SetData(boidSettings);
            
            m_ComputeShader.SetInt("numBoids", Boids.Count);
            m_ComputeShader.SetFloat("deltaTime", Time.deltaTime);
            m_ComputeShader.SetBuffer(0, "boids", boidBuffer);
            m_ComputeShader.SetBuffer(0, "settings", settingsBuffer);

            int threadGroups = Mathf.CeilToInt(Boids.Count / (float) THREAD_GROUP_SIZE);
            m_ComputeShader.Dispatch(0, threadGroups, 1, 1);

            boidBuffer.GetData(boidDatas);

            for (int i = 0; i < Boids.Count; i++)
            {
                Boids[i].Data = boidDatas[i];
                Boids[i].transform.forward = Boids[i].Data.Velocity / Boids[i].Data.Velocity.magnitude;
                Boids[i].transform.position = Boids[i].Data.Position;
            }

            boidBuffer.Release();
            settingsBuffer.Release();
        }
    }
}