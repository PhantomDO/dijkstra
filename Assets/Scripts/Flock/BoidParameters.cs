using System.Text;
using UnityEngine;
using UnityEngine.UIElements;

namespace Flocking
{
    [System.Serializable]
    public struct BoidSettings
    {
        [Header("Weight")]
        public float WeightCohesion;
        public float WeightSeparation;
        public float WeightAlignment;
        
        [Header("Radius")]
        public float RadiusAvoid;
        public float RadiusVision;

        [Header("Force")]
        public float MaxForce;
        public float MinSpeed;
        public float MaxSpeed;
        
        public static int Size() => sizeof(float) * 8;

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"BoidSettings : {{\n");
            sb.Append($"\tWeightCohesion: {WeightCohesion},\n");
            sb.Append($"\tWeightSeparation: {WeightSeparation},\n");
            sb.Append($"\tWeightAlignment: {WeightAlignment},\n");
            sb.Append($"\tRadiusAvoid: {RadiusAvoid},\n");
            sb.Append($"\tRadiusVision: {RadiusVision},\n");
            sb.Append($"\tMaxForce: {MaxForce},\n");
            sb.Append($"\tMinSpeed: {MinSpeed},\n");
            sb.Append($"\tMaxSpeed: {MaxSpeed},\n}}\n");
            return sb.ToString();
        }
    }

    [CreateAssetMenu]
    public class BoidParameters : ScriptableObject
    {
        [Header("Base")]
        public BoidSettings Settings;

        [Header("Target")]
        public float WeightTarget;
        public float FoV = 90f;
        
        [Header("Collision")] 
        public float WeightCollision;
        public float RadiusOverlap;
        public float DistanceAvoid;
        public LayerMask LayerOverlap;
    }
}