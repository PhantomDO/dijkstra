﻿using StateMachine;
using UnityEngine;

namespace Flocking
{
    [CreateAssetMenu(fileName = "Wander", menuName = "States/Movements/Wander")]
    public class WanderMovementBoidState : MovementBoidState
    {
        public override State Do()
        {
            Fsm.Actor.TargetDir = Vector3.zero; 
            return base.Do();
        }
    }
}