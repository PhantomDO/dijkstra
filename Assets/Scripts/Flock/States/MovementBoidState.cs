﻿using UnityEngine;
using StateMachine;

namespace Flocking
{
    [System.Serializable]
    public enum BoidMovementEnum : uint
    {
        Wander,
        Focus,
        Flee,
    }

    public abstract class MovementBoidState : State<BoidMovementStackStateMachine>
    {
        [SerializeField] protected BoidMovementEnum m_State;
        public override uint Get() => (uint)m_State;

        protected Vector3 m_Direction;
        protected bool m_InRadius;

        public override State Do()
        {
            m_Direction = Fsm.Actor.Target.position - Fsm.Actor.Data.Position;
            m_Direction.y = Camera.main.orthographic ? 0.0f : m_Direction.y;

            var sqrDistanceAvoid = Fsm.Actor.Parameters.DistanceAvoid * Fsm.Actor.Parameters.DistanceAvoid;
            m_InRadius = m_Direction.sqrMagnitude < sqrDistanceAvoid;
            
            BoidMovementEnum movement = BoidMovementEnum.Wander;
            if (FlockManager.Instance != null && FlockManager.Instance.Hunter != null)
            {
                var hunter = FlockManager.Instance.Hunter;
                movement = hunter.InFovList.Contains(Fsm.Actor) && m_InRadius
                    ? BoidMovementEnum.Flee : BoidMovementEnum.Focus;
            }
            
            return Fsm.SetState((uint)movement);
        }
    }
}