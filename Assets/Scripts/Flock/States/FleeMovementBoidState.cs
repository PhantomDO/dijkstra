﻿using StateMachine;
using UnityEngine;

namespace Flocking
{
    [CreateAssetMenu(fileName = "Flee", menuName = "States/Movements/Flee")]
    public class FleeMovementBoidState : MovementBoidState
    {
        public Gradient FleeGradient;

        public override State Do()
        {
            Fsm.Actor.TargetDir = -m_Direction;
            var mesh = Fsm.Actor.transform.GetChild(0);
           
            if (mesh.TryGetComponent(out MeshRenderer mr))
            {
                var color = mr.material.color;
                color.a = .25f;
                mr.material.color = color;
            }

            if (Fsm.Actor.TryGetComponent(out TrailRenderer tr))
            {
                tr.colorGradient = FleeGradient;
            }

            return base.Do();
        }
    }
}