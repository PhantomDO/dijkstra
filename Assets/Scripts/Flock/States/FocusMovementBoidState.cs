﻿using StateMachine;
using UnityEngine;

namespace Flocking
{
    [CreateAssetMenu(fileName = "Focus", menuName = "States/Movements/Focus")]
    public class FocusMovementBoidState : MovementBoidState
    {
        public Gradient FocusGradient;

        public override State Do()
        {
            Fsm.Actor.TargetDir = m_Direction;
            var mesh = Fsm.Actor.transform.GetChild(0);
           
            if (mesh.TryGetComponent(out MeshRenderer mr))
            {
                var color = mr.material.color;
                color.a = 1f;
                mr.material.color = color;
            }

            if (Fsm.Actor.TryGetComponent(out TrailRenderer tr))
            {
                tr.colorGradient = FocusGradient;
            }

            return base.Do();
        }
    }
}