﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Flocking.Gameplay
{
    public class Hunter : MonoBehaviour
    {
        [SerializeField] private float m_MoveSpeed;
        [SerializeField] private float m_MinMoveSpeed;
        [SerializeField] private float m_MaxMoveSpeed;

        [SerializeField] private GameObject m_Bullet;
        [SerializeField] private Transform m_Gun;
        [SerializeField] private float m_MinGunDistance;
        [SerializeField] private float m_MaxGunDistance;
        [SerializeField] private Light m_Muzzle;
        [SerializeField] private float m_FireRate;
        private float m_NextFireRate;

        [SerializeField, Range(10f, 359f)] private float m_MinFov;
        [SerializeField, Range(10f, 359f)] private float m_MaxFov;
        [SerializeField] private float m_MinFovRadiusVision;
        [SerializeField] private float m_MaxFovRadiusVision;

        private Vector2 m_MoveAxis, m_LookAxis;
        private Vector3 m_ClampScreenMin, m_ClampScreenMax;

        public List<Boid> InFovList { get; } = new List<Boid>();

        public Vector3 GunDirection
        {
            get
            {
                var dir = (m_Gun.position - transform.position);
                dir.y = Camera.main.orthographic ? 0 : dir.y;
                return dir;
            }
        }

        private void Start()
        {
            if (m_Gun == null) m_Gun = transform.GetChild(0);
            m_ClampScreenMin = Camera.main.ScreenToWorldPoint(Screen.safeArea.min);
            m_ClampScreenMax = Camera.main.ScreenToWorldPoint(Screen.safeArea.max);
        }

        private void FixedUpdate()
        {
            Vector3 movement = new Vector3(m_MoveAxis.x, 0, m_MoveAxis.y);
            Vector3 hunterPosition = ClampedPosition(
                movement * m_MoveSpeed * Time.deltaTime, 
                m_ClampScreenMin, m_ClampScreenMax);
            transform.position += hunterPosition;

            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
            var gunDirection = mousePosition - transform.position;
            gunDirection.y = Camera.main.orthographic ? 0 : gunDirection.y;
            m_Gun.position = transform.position + ClampMagnitude(gunDirection, m_MinGunDistance, m_MaxGunDistance);

            InFovList.Clear();
            if (FlockManager.Instance?.Boids?.Count > 0)
            {
                foreach (var boid in FlockManager.Instance.Boids)
                {
                    if (InFov(boid.transform.position) && !InFovList.Contains(boid))
                    {
                        InFovList.Add(boid);
                    }
                }
            }
            
            m_Muzzle.intensity = 0f;
            if (Mouse.current.leftButton.isPressed && Time.time - m_NextFireRate >= m_FireRate)
            {
                var bullet = GameObject.Instantiate(m_Bullet, transform);
                bullet.transform.localScale = new Vector3(0.1f, 10f, 0.1f);
                bullet.transform.SetPositionAndRotation(m_Gun.position, Quaternion.LookRotation(
                    GunDirection.normalized, Vector3.up));

                m_NextFireRate = Time.time + m_FireRate;
                m_Muzzle.intensity = 100f;
            }
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            // FOV Far
            {
                var angle = m_MinFov / 2;
                var qMin = Quaternion.AngleAxis(-angle, transform.up);
                var qMax = Quaternion.AngleAxis(angle, transform.up);
                var minFovPos = transform.position + (qMin * GunDirection.normalized * m_MinFovRadiusVision);
                var maxFovPos = transform.position + (qMax * GunDirection.normalized * m_MinFovRadiusVision);
                
                Gizmos.color = Color.red;
                Gizmos.DrawLine(transform.position, minFovPos);
                Gizmos.color = Color.cyan;
                Gizmos.DrawLine(transform.position, maxFovPos);
                var dotMinFov = Vector3.Dot(GunDirection.normalized, (minFovPos - transform.position).normalized);
                var dotMaxFov = Vector3.Dot(GunDirection.normalized, (minFovPos - transform.position).normalized);
                
                Gizmos.color = Color.green;
                Gizmos.DrawLine(transform.position, transform.position + GunDirection.normalized * m_MinFovRadiusVision);
            }

            // Fov near
            {
                var angle2 = m_MaxFov / 2;
                var qMin2 = Quaternion.AngleAxis(-angle2, transform.up);
                var qMax2 = Quaternion.AngleAxis(angle2, transform.up);
                var minFovPos2 = transform.position + (qMin2 * GunDirection.normalized * m_MaxFovRadiusVision);
                var maxFovPos2 = transform.position + (qMax2 * GunDirection.normalized * m_MaxFovRadiusVision);
                
                Gizmos.color = Color.red;
                Gizmos.DrawLine(transform.position, minFovPos2);
                Gizmos.color = Color.cyan;
                Gizmos.DrawLine(transform.position, maxFovPos2);
                var dotMinFov2 = Vector3.Dot(GunDirection.normalized, (minFovPos2 - transform.position).normalized);
                var dotMaxFov2 = Vector3.Dot(GunDirection.normalized, (minFovPos2 - transform.position).normalized);
                
                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(transform.position, m_MaxFovRadiusVision);
            }
        }
#endif

        private Vector3 ClampedPosition(Vector3 position, Vector3 min, Vector3 max)
        {
            float x = Mathf.Clamp(position.x, min.x, max.x);
            float y = Mathf.Clamp(position.y, min.y, max.y);
            float z = Mathf.Clamp(position.z, min.z, max.z);
            return new Vector3(x, Camera.main.orthographic ? 0 : y, z);
        }
        
        public Vector3 ClampMagnitude(Vector3 vector, float min, float max)
        {
            double sm = vector.sqrMagnitude;
            if (sm > (double) max * (double) max) return vector.normalized * max;
            else if (sm < (double) min * (double) min) return vector.normalized * min;
            return vector;
        }

        public void Move(InputAction.CallbackContext context)
        {
            m_MoveAxis = context.action.ReadValue<Vector2>();
        }

        public void Look(InputAction.CallbackContext context)
        {
            if (Math.Abs(context.action.ReadValue<Vector2>().sqrMagnitude - Vector2.zero.sqrMagnitude) > Mathf.Epsilon)
            {
                m_LookAxis = context.action.ReadValue<Vector2>();
            }
        }

        public bool InFov(Vector3 other)
        {
            if (m_MinFov <= Mathf.Epsilon || m_MaxFov <= Mathf.Epsilon) return false;
            if (m_MinFovRadiusVision <= Mathf.Epsilon || m_MaxFovRadiusVision <= Mathf.Epsilon) return false;
            
            float angle;
            float angleFov;
            var dir = (other - transform.position);
            if (dir.sqrMagnitude < m_MaxFovRadiusVision * m_MaxFovRadiusVision)
            {
                angle = m_MaxFov / 2;
                angleFov = Vector3.Angle(GunDirection.normalized, dir.normalized);
                return angle > angleFov && dir.sqrMagnitude < m_MaxFovRadiusVision * m_MaxFovRadiusVision;
            }
            else if (dir.sqrMagnitude < m_MinFovRadiusVision * m_MinFovRadiusVision)
            {
                angle = m_MinFov / 2;
                angleFov = Vector3.Angle(GunDirection.normalized, dir.normalized);
                return angle > angleFov && dir.sqrMagnitude < m_MinFovRadiusVision * m_MinFovRadiusVision;
            }

            return false;
        }
    }
}