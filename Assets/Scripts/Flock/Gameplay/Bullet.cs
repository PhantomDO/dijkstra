﻿using System.Security.Cryptography;
using UnityEngine;

namespace Flocking.Gameplay
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private float m_MoveSpeed = 50f;
        [SerializeField] private float m_LifeSpan = 5f;

        private float m_LifeStart;

        private void Start()
        {
            m_LifeStart = Time.time;
        }

        private void Update()
        {
            transform.position += transform.forward * m_MoveSpeed * Time.deltaTime;

            if (Time.time - m_LifeStart >= m_LifeSpan)
            {
                Destroy(gameObject);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out Boid boid))
            {
                FlockManager.Instance?.Boids?.Remove(boid);
                Destroy(boid.gameObject);
            }

            Destroy(gameObject);
        }
    }
}